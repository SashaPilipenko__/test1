import styled from 'styled-components';

const Col4 = styled.div`
@media (min-width: 992px){
    -webkit-box-flex: 0;
    max-width: 33.3333%;
  flex: 0 0 33.3333%;
}
`;

export {Col4};