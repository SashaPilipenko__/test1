import {call,put,takeLatest} from 'redux-saga/effects';
import {fetchProductsPending,
    fetchUpdatePost,
    fetchCreate,
    fetchProductsSuccess,
     fetchProductsError,
     fetchDeletePost} from '../actions';

const URL = 'https://jsonplaceholder.typicode.com/posts';

function* mySaga(){
    yield takeLatest(['POST_FETCH'],fetchPost);
    yield takeLatest(['POST_DELETE'],deletePost);
    yield takeLatest(['POST_EDIT'],updatePost);
    yield takeLatest(['POST_ADD'],addPost);
}

function* fetchPost(action){
    const URLITEM = (action.payload) ? `${URL}/${action.payload}` : URL;
    try{
        yield put(fetchProductsPending());
       const post = yield call(() => fetch(URLITEM)
                                     .then(res => res.json()));
       yield put(fetchProductsSuccess(post));
    }
    catch(error){
        yield put(fetchProductsError());
    }
}

function* deletePost(action){
    const URLITEM =  `${URL}/${action.payload}` ;
    try{
       const post = yield call(() => fetch(URLITEM,{
        method: 'DELETE'
      })
        .then(res => res.json()));
       yield put(fetchDeletePost(post.id));
    }
    catch(error){
        yield put(fetchProductsError());
    }
}

function* updatePost({payload}){
    const URLITEM =  `${URL}/${payload.id}`;
    try{
       const post = yield call(() => fetch(URLITEM, {
        method: 'PUT',
        body: JSON.stringify({
          id: payload.id,
          title: payload.title,
          body: payload.body,
          userId: 1
        }),
        headers: { 
          "Content-type": "application/json; charset=UTF-8"
        }
      })
                                     .then(res => res.json()));
       yield put(fetchUpdatePost(post));
    }
    catch(error){
        yield put(fetchProductsError(error));
    }
}

function* addPost({payload}){
    try{
        const post = yield call(() => fetch(URL, {
            method: 'POST',
            body: JSON.stringify({
              title: payload.title,
              body: payload.body,
              userId: 1
            }),
            headers: {
              "Content-type": "application/json; charset=UTF-8"
            }
          }).then(response => response.json()));
          yield put(fetchCreate(post));
    }
    catch(error){
        yield put(fetchProductsError(error));
    }
    
}

export default mySaga;