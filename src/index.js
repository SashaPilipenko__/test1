import React from 'react';
import ReactDOM from 'react-dom';
import { createStore,applyMiddleware  } from 'redux';
import { Provider } from 'react-redux';
import {reducer} from './reducer';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import mySaga from './sagas/sagas';

import './index.css';
import App from './components/app/App';

const sagaMiddleware = createSagaMiddleware();
const store = createStore(reducer,applyMiddleware(thunk,sagaMiddleware));

ReactDOM.render(
<Provider store={store}>
<App />
</Provider>, document.getElementById('root'));

sagaMiddleware.run(mySaga);
