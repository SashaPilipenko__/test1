import React,{Component} from 'react';
import {Redirect} from 'react-router-dom';
import {connect} from 'react-redux';
import Spinner from '../spinner/spinner';
import {PostAdd} from './post-add';
import PostEdit from './post-edit';
import {postFetch} from '../../actions';

 class Profile extends Component{
    componentDidMount() {
      setTimeout (() => {
        this.props.dispatch(postFetch());
      });
       }
    Onlogout = () => {
        localStorage.login = 'false';
        this.forceUpdate();
    }
    render(){
        let body;
        const {posts,load,dispatch} = this.props;
        const spin = (load) ? <Spinner/> : null;
        if(localStorage.login !== 'false'){
           body = (
            <div>
            <div >
            <h1>Your profile!</h1> 
            <button onClick={this.Onlogout}>Logout</button>
            </div>
           <PostAdd dispatch={dispatch} />
           
            <ul className="row">{spin}{
                
                posts.map(item=> (<PostEdit key={item.title} item={item} />))
            }
            </ul>
        </div>
           );
      }else{
          body = (<Redirect to="/login" />);
      }

        return body;
    }
    
}

const mapStateToProps = (state) => {
    return {
      posts : state.posts,
      load: state.pending
    }
  }
  
 /* const mapDispatchToProps = (dispatch) => {
    const actions = bindActionCreators({fetchPostsAction,fetchDelete,fetchUpdate,fetchCreateActive},dispatch);
    return {...actions};
  };
  */
  
  
  export default connect(mapStateToProps)(Profile);