import React from 'react';
import {Redirect} from 'react-router-dom';
import {Form} from '../../styled-components/form';

export default class Login extends React.Component {
    state={
        login:'',
        password:''
    }
 
     Onlogin = (e) => {
         e.preventDefault();
         if(this.state.login === 'sasha' && this.state.password === '123'){
            localStorage.login = 'true';
         }
        this.setState({
            login:'',
            password:'',
            badway:'Incorrect password or email'
        });
    }
    
    onChange = (e) => {
        const {name,value} = e.target;
        this.setState({
            [name] : value
        });
    }

    render(){
        let body;
        const {login,password} = this.state;
        
        if(localStorage.login !== 'true'){
            body = ( <Form onSubmit={this.Onlogin}>
                    <input className="form-control" placeholder="Login" name="login" type="text" onChange={this.onChange} value={login}/>
                    <input className="form-control" placeholder="Password" name="password" type="password" onChange={this.onChange} value={password} />
                    <button className="btn btn-primary" type="submit">Login</button>
                    {this.state.badway}
            </Form> );
            
        }else{
            body = (
                <Redirect to="/profile" />
            );
        }
           return  body;
    
        }
    
}