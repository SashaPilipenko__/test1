import React,{Component} from 'react';
import {connect} from 'react-redux';
import Spinner from '../spinner/spinner';
import {Post} from '../post/post';
import {postFetch} from '../../actions';
 class Posts extends Component{
    componentDidMount() {
    setTimeout (() => {
      this.props.dispatch(postFetch());
    });
  }

       render(){
        const {posts,load} = this.props;
        const spin = (load) ? <Spinner/> : null;
        return(
            <div className="row" >
              {spin}
            {posts.map(({title,id,body}) => <Post key={title} id={id} body={body} title={title}/> )}
            </div>  
        );
       }
}

const mapStateToProps = (state) => {
    return {
      posts : state.posts,
      load: state.pending
    }
  }
  
 /*const mapDispatchToProps = (dispatch) => {
    const actions = bindActionCreators({fetchPostsAction},dispatch);
    return {...actions};
  };
   */
  
  export default connect(mapStateToProps)(Posts);