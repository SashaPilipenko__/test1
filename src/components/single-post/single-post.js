import React from 'react';
import {connect} from 'react-redux';
import Spinner from '../spinner/spinner';
import {postFetch} from '../../actions';

 class SinglePost extends React.Component{
    componentDidMount(){
        const { activeId} = this.props;
        setTimeout (() => {
            this.props.dispatch(postFetch(activeId));
          });
    }
    render(){
        const {post,load} = this.props;
        if(load) {
        return <div className="jumbotron jumbotron-fluid"> <Spinner/> </div> };
        return(
            
            <div className="jumbotron jumbotron-fluid">
                <div className="container">
                    <h1 className="display-4">{post.title}</h1>
                    <p className="lead">{post.body}</p>
                </div>
            </div>
        );
    }
}

const mapStateToProps = ({singlePost,pending}) => {
    return {
      post : singlePost,
      load: pending
    }
  }
  
/*  const mapDispatchToProps = (dispatch) => {
    const actions = bindActionCreators({fetchSinglePostAction},dispatch);
    return {...actions};
  };*/

  export default connect(mapStateToProps)(SinglePost);