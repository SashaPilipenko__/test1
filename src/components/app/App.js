import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
} from "react-router-dom";
import Header from '../header/header';
import Posts from '../posts/posts';
import Profile from '../profile/profile';
import Login from '../login/login';
import Main from '../main/main';
import SinglePost from '../single-post/single-post';

import './App.css';


class App extends React.Component{

  render(){
      return (
      <Router>
         <Header/>
          <div className="App container">
          
            <Switch>

              <Route path="/" exact component={Main}>
              </Route>

              <Route path="/posts" exact component={Posts} />

              <Route path="/profile" exact component={Profile} />
               

              <Route path="/login" exact component={Login} />
                

              <Route path="/posts/:id"  render={({match})=> <SinglePost activeId={match.params.id}/>}/>
            
            </Switch> 
         </div>
      </Router>
    );
  }
}

export default App;
